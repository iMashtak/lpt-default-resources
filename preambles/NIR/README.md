# Преамбула для шаблона УИР, НИР, ВКР

> Автор: Маштак Иван Андреевич
> * Gitlab: [iMashtak](https://gitlab.com/iMashtak)
> 
> Основана на исходном коде данного проекта: [skibcsit/thesis-template](https://gitlab.com/skibcsit/thesis-template).

- `preamble-old` - преамбула, совместимая с наработками из первоисточника: [skibcsit/thesis-template](https://gitlab.com/skibcsit/thesis-template).
- `preamble` - целевая версия преамбулы. Отличия:
    - структуризация и комментирование разделов самого файла преамбулы;
    - поддержка набора опций сборки:
        - `UseTimesNewRomanFont = true | false` - использовать набор шрифтов, базирующихся на *Times New Roman* (по-умолчанию `true`);
        - `UseLinuxLibertineFont = true | false` - использовать набор шрифтов, базирующихся на *Linux Libertine* (по-умолчанию `false`, не применяется, если `UseTimesNewRomanFont=true`);
        - `UseSolidPictureNumbering = true | false` - использовать сплошную нумерацию рисунков в документе (по-умолчанию `false` - нумерация рисунков производится относительно `chapter`);
        - `SwitchState = on | off | both` - регулирует правило использования команды `\switch{}{}`:
            - `on` - в текст подставляется контент из первого аргумента команды;
            - `off` - в текст подставляется контент из второго аргумента команды;
            - `both` - в текст подставляется контент из обеих аргументов команды, последовательно первый, а затем второй.
    - поддержка стилизации исходного кода для языков `Dockerfile` и `js`.